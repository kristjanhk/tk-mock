\section{Veebirakenduse ülevaade ja arhitektuur} \label{sec:product}

Järgnevas peatükis kirjeldatakse töö raames loodava veebirakenduse eesmärki,
näidatakse kasutajaliidese ja serveripoolset funktsionaalsust, töövooge, kuvasid ning arhitektuuri.
Samuti tuuakse välja mittefunktsionaalsed nõuded rakendusele ning kirjeldatakse selle majutust testkeskkonnas.

\subsection{Eesmärk} \label{subsec:solution-goal}

Töö raames loodava veebirakenduse eesmärk on lahendada 1.~peatükis kirjeldatud probleemi,
kus töötukassa infosüsteemi viiakse järk-järgult monoliitselt arhitektuurilt mikroteenuste arhitektuurile,
mis on suurendanud märkimisväärselt moodulitevahelist suhtlust ja vajadust nende integratsiooni testida.
Veebirakenduse arendusel on lähtutud 2.~peatükis kirjeldatud nõuetest.
Kui eelnevalt kirjeldatud lahendused katsid ainult osaliselt EMPIS-e projektis teenuste testimise jaoks
vajalikku funktsionaalsust, siis loodav rakendus on kohandatud vastavalt nõuetele.
Kuigi loodav veebirakendus asendab olemasolevaid vahendeid, ei saa eeldada,
et kasutajad asuvad kohe uut lahendust kasutama.
Seega ei tohi selle kasutusele võtmine takistada varasemate tööriistade kasutamist,
vaid peab pakkuma mugavamat ja kiiremat kasutajakogemust võrreldes varasemate vahenditega.

\subsection{Kasutajaliidese funktsionaalsuse kirjeldus} \label{subsec:functionality-frontend}

Kasutajaliides sisaldab järgmist funktsionaalsust:

Rakenduse kasutamine on isikupõhine.
Ühe kasutaja tegevused ei tohi teisi segada ega nende tööd kuidagi häirida.
See tähendab, et iga testija või arendaja peab olema testkeskkonda sisse logitud oma kasutajakontoga,
mida ei tohi omavahel jagada.
Seega suudab rakendus kasutajaid autentida läbi testkeskkonna tsentraalse autentimisteenuse \gls{CAS}\@.
Erandiks on süsteemne kasutaja, mida töötukassa rakendused kasutavad näiteks taustatöödel päringute sooritamiseks.
Selliste olukordade testimise jaoks peab olema võimalik sisse logida ka süsteemse kasutajaga.

Rakenduse funktsionaalsust on võimalik isikupõhiselt sisse ja välja lülitada.
Rakenduse kasutamise ajal suunatakse kõik \gls{HTTP}-päringud läbi serveri,
mis võib teha liikluse märgatavalt aeglasemaks või tekitada vigu, mida päriselt ei tohiks tekkida.
Seega peab olema võimalus päringute saatmine rakendusse välja lülitada.

Teenused on grupeeritud väliste süsteemide ning X-tee registrite põhiselt.
Kui kasutaja testib mõnda funktsionaalsust töötukassa rakenduses,
siis analüütiku koostatud dokumentatsioonist tuleb välja, milliste rakenduste teenuseid seal kasutatakse.
Seega on päringud jaotatud rakenduste järgi, et neid oleks kerge leida.
Rakendused on omakorda jaotatud koostööpartnerite järgi, kes neid arendavad.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{assets/image/frontend-teenused}
    \caption{Teenuste kuva}
    \label{fig:frontend-teenused}
\end{figure}

Päringute ja vastuste seadistamine on grupeeritud teenusepõhiselt.
Ühel rakendusel võib olla palju teenuseid ja neid peab saama ükshaaval seadistada.
Joonisel~\ref{fig:frontend-teenused} on toodud näide teenuste kuvast.
Teenusel on selgelt eristav tüüp, \gls{HTTP}-meetod, aadress ja kasutaja saab lisada enda märkmeid.
Kui kasutaja on seadistanud mitu teenust, siis saab neid filtreerida eelnevalt kirjeldatud tunnuste järgi.
Kasutaja saab teenuseid juurde lisada, muuta ja kustutada,
sest need ei ole fikseeritud, vaid võivad igal hetkel muutuda.
Rakendusse saab neid lisada ka enne seda, kui need on päriselt valmis arendatud.
Uue teenuse lisamisel ning aadressi valimisel pakub rakendus kasutajale juba teadaolevaid aadresse,
millega on võimalik ära hoida väärtuste valesti sisestamist.

Rakenduses saab teenustele seadistada vastuseid, millega rakendus teenuse sissetulevale päringule vastab.
Neid saab lisada, muuta ning kustutada.
Seadistatud vastust saab sisse ja välja lülitada.
Kui vastus on välja lülitatud, saadab rakendus sissetuleva päringu edasi välisesse rakendusse
ning vastab sissetulevale päringule sealt saadud vastusega.
Ühel teenusel võib korraga olla mitu seadistatud vastust, et kasutaja saaks vastuseid kergelt vahetada.
Vastuse juures kuvatakse kasutaja sisestatud märkmeid, loomishetke, lühidalt eeldatava päringu ja vastuse keha.
Samuti on võimalik vaadata rakendusest läbi käinud liiklust ühe vastuse kohta.

Vastuse seadistamisel saab kirjeldada sellele eelnevat päringut.
Päringul saab seadistada \gls{HTTP}-aadressi parameetreid, päiseid, keha tüüpi ja selle sisu.
Lisaks päringu kirjeldamisele saab vastuse seadistamisel kirjeldada ka vastust ennast.
Joonisel~\ref{fig:frontend-request-response} on näha näidet päringu ja vastuse seadistamisest.
Kasutaja saab vastusele määrata enda märkmeid, viivitust, \gls{HTTP}-staatust, -päiseid, keha tüübi ja selle sisu.
Nii päringu kui ka vastuse keha tüübiks võib olla vabavormis tekst, \gls{JSON} või \gls{XML} ning
keha sisu korrektsust valideeritakse vastavalt valitud keha tüübile.
Uue päringu ja vastuse loomisel proovib rakendus need automaatselt ära seadistada juhul,
kui rakendus on teistel kasutajatel juba näinud sama teenuse liiklust.
Kui vähemalt pooltel nähtud päringutest või vastustest on mingi parameetri võti samasuguse väärtusega,
täidetakse see automaatselt kasutaja eest ära.
Samuti on kasutajal võimalik vastuse seadistamise lihtsustamiseks teha päring välisesse rakendusse,
saada sealt näidisvastus päris andmetega ning selle põhjal soovitud vastus seadistada.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{assets/image/frontend-request-response-large-borders}
    \caption{Modaalis päringu ja vastuse seadistamise}
    \label{fig:frontend-request-response}
\end{figure}

Kasutajaliideses on näha ka senine liiklus.
Seda näeb nii üle ühe rakenduse, ühe teenuse kui ka ühe seadistatud vastuse.
Kuna rakendusest võib palju päringuid läbi käia, siis kuvatakse need nimekirjana
ning on filtreeritavad \gls{HTTP}-aadressi, -staatuse, päringu ja vastuse keha ning kasutaja enda märkmete järgi.
Samuti peab iga läbikäinud päringu kohta nägema detaile samasugusel kujul nagu vastuse seadistamisel.
Läbi käinud päringu järgi saab seadistada uut vastust, millega rakendus järgmistele päringutele vastaks.

\subsection{Serveri funktsionaalsuse kirjeldus} \label{subsec:functionality-backend}

Lisaks kasutajaliidesele sisaldab rakenduse serveri pool järgnevat funktsionaalsust:

Server võimaldab kasutajal ennast autentida läbi tsentraalse autentimisteenuse \gls{CAS}\@.
See tähendab, et testimisrakendusse ei ole vaja eraldi sisse logida,
kui kasutaja on eelnevalt mõnda töötukassa rakendust külastanud.
Selline lahendus sarnaneb suures osas \glslink{OAUTH}{OAuth} standardile.
Autentimise jaoks suunatakse kasutaja \gls{CAS}-i sisselogimislehele,
kus sisse logides suunatakse kasutaja tagasi rakendusse koos piletiga, mille abil saab server \gls{CAS}-ist kontrollida,
kas kasutaja on edukalt sisse loginud.
Sellise sisselogimise eeliseks on, et kasutaja ei pea rakendusse oma parooli sisestama,
vaid sellega tegeleb \gls{CAS} ise.
Erandiks on süsteemne kasutaja, mille saab kasutajaliidese kaudu valida alles peale tavakasutajana sisselogimist.
Kõik serveri teenused, mis pole seotud kasutaja autentimisega, nõuavad kasutaja eelnevat autentimist.
Autentimisel päritakse kasutaja nimi töötukassa isikute moodulist, kus hoitakse isikute andmeid.

Server võimaldab testkeskkonna rakenduste päringute suunamist tema pihta sisse ja välja lülitada.
Sisselülitamiseks suhtleb rakendus töötukassa süsteemse mooduliga
ning määrab süsteemse parameetri \emph{TEST\_REROUTE\_{isikukood}} väärtuseks rakenduse enda aadressi.
Süsteemne moodul edastab muudatused töötukassa rakendustele üle \glslink{hazelcast}{Hazelcasti klastri},
mille abil need tuvastavad, kas antud isiku päringud on vaja suunata ümber testimisrakendusse.
Funktsionaalsuse väljalülitamise jaoks määratakse vastav parameeter kehtetuks.

Serveri kaudu on võimalik nii teenuseid kui ka kasutaja seadistatud päringuid ja vastuseid leida,
lisada, muuta ja kustutada andmebaasist.
Kõik sisendid valideeritakse nii rakenduse kui ka andmebaasi poolel.
Teenuste puhul on võimalik soovitusi pärida sisestatud \gls{HTTP}-aadressi järgi.
Kasutaja seadistatud päringute ja vastuste puhul on võimalik saada näidispäringuid ja -vastuseid.

Kui kasutaja on seadistanud vastuse mingile teenuse päringule ja selle sisse lülitanud,
siis suudab server vastavale sissetulevale \gls{HTTP}-päringule sellega vastata.
Kui kasutaja on seadistanud päringu, siis oskab server sellega välisesse rakendusse päringut teha
ning tagasi saadud vastust salvestada.
Kui sissetulev \gls{HTTP}-päring on hoopis selline, mida kasutaja pole seadistanud,
suudab server seda välisesse rakendusse edasi saata ning tagasitulnud vastusega pärijale vastata.
Päringule seadistatud vastusega vastamine
ja selle edasisuunamise töövood on välja toodud joonisel~\ref{fig:reroute-diagram}.

\begin{figure}[H]
    \centering
    \makebox[0pt]{\includesvg[inkscapelatex=false, width=1.2\textwidth]{assets/image/reroute-respond}}
    \caption{Sissetulevale päringule vastamine või selle edasisuunamine}
    \label{fig:reroute-diagram}
\end{figure}

Eelmises lõigus kirjeldatud tegevuste käigus salvestatakse kõik päringud ja vastused andmebaasi,
et neid oleks võimalik hiljem leida.
Samuti on oluline need logida nii konsooli kui ka tsentraalsesse logimisrakendusse Graylog,
kuhu logivad kõik töötukassa rakendused.
Testija töövoog võib läbida mitut rakendust ja on oluline, et logisid vaadates oleksid kõik tegevused esindatud.
Kui sissetulevas päringus on kaasas jälitamisidentifikaator, tuleb seda logimisel kasutada
ja edasisaadetavate päringutega kaasa panna.
Jälitamisidentifikaatori abil saab testija enda töövoogu logidest kergelt üles leida.
Logitav päring ja vastus on selge struktuuriga, kust on näha \gls{HTTP}-meetod, -aadress ja -parameetrid,
kasutaja isikukood, päised ning keha ja vastuse puhul ka staatuse kood.
Joonisel~\ref{fig:log-diagram} on näha päringu ja vastuse logisid.

\begin{figure}[H]
    \centering
    \makebox[0pt]{\includesvg[inkscapelatex=false, width=1.3\textwidth]{assets/image/log}}
    \caption{Päringu ja vastuse logimine}
    \label{fig:log-diagram}
\end{figure}

Server peab koostama korrektseid \gls{REST}-päringuid ja -vastuseid.
Päringute sooritamiseks töötukassa rakendustesse peab
rakendus suutma genereerida valiidse \glslink{JWT}{JSON Web Tokeni}, mida välja kutsutavad rakendused aktsepteerivad.
X-tee teenustega suhtlemiseks peab server suutma koostada korrektse \gls{SOAP}-päringu ja vastuse koos manustega.

\subsection{Mittefunktsionaalsed nõuded} \label{subsec:nonfunctional-requirements}

Rakendus ei tohi testimist keerulisemaks teha ehk ei tohi tekitada juurde vigu,
mida ilma selle kasutamiseta ei esine.
Selleks on rakendus kaetud ühik- ja integratsioonitestidega.

Rakendus võib sattuda suure koormuse alla,
sest töötukassa rakendused teevad palju päringuid nii üksteise vahel kui ka \glslink{xroad}{X-tee} registritesse.
Kuigi testimisel on korrektsus olulisem kui kiirus,
ei tohiks see testija tööprotsessi oluliselt aeglasemaks teha.
Selle jaoks on rakendus kaetud koormustestidega, mille väljund on näha lisades jooniselt~\ref{fig:backend-stress}.
Testide tulemusest on näha, et testkeskkonnas simuleerides paralleelselt 10-ne kasutaja päringuid
suutis rakendus töödelda 201,6 päringut sekundis vastates 95\% juhtudel 32,5 millisekundi jooksul.

Rakendus peab olema kasutajale kergelt arusaadav.
Rakendust hakkavad kasutama peamiselt arendajad ja testijad.
Selle jaoks on kasutajaliides ehitatud samasuguse stiili ja komponentidega nagu töötukassa rakendused.

Rakendus peab olema arendajale kergelt edasiarendatav.
Selle jaoks on kasutatud kasutajaliidese
ja serveri poolel teiste töötukassa rakendustega samu või sarnaseid tehnoloogiaid ja võtteid.

\newpage

\subsection{Rakenduse arhitektuur} \label{subsec:architecture}

Järgnevates alapeatükkides on lähemalt välja toodud tehnoloogiad,
millega on kasutajaliides ja serveripoolsed rakendused ehitatud.

\subsubsection{Kasutajaliides veebiraamistikuga Angular} \label{subsubsec:architecture-frontend}

Kasutajaliides on ehitatud raamistikule Angular~\cite{angular}.
See on vabavaraline Google’i arendatav veebiraamistik kasutajaliidese loomiseks.
Autor kasutas Angulari 8.~versiooni.
Angular võimaldab rakenduse funktsionaalsust hoida eraldiseisvates komponentides,
mis on omakorda jaotatud erinevateks kihtideks.
Komponentide ja kihtide eelis on see,
et see teeb rakenduse koodi paremini hallatavaks, taaskasutatavaks ja testitavaks.
Programmikood on kirjutatud keeles TypeScript, mis põhineb keelel JavaScript,
lisades sellele juurde staatilise tüüpimissüsteemi ja funktsionaalsust.
Väljakuvatavad elemendid on kirjeldatud \gls{HTML}-failidena ning stiliseeritud \gls{SCSS}-failidega.
Kasutajaliideses kasutatavad tehnoloogiad on välja toodud joonisel~\ref{fig:frontend-tech}.

\begin{figure}[H]
    \centering
    \includesvg[inkscapelatex=false, width=\textwidth]{assets/image/frontend-tech}
    \caption{Kasutajaliideses kasutatavad tehnoloogiad}
    \label{fig:frontend-tech}
\end{figure}

Üks rakenduse kuva võib koosneda paljudest komponentidest, mis on jaotatud esitlus-, teenuse- ja liidestuskihiks.
Esitluskiht koosneb TypeScript- ja \gls{HTML}-failist ning sisaldab endas elemente,
mida välja kuvatakse, ja äriloogikat, kuidas need välja kuvatakse.
Liidestuskiht võimaldab \gls{REST}-päringutega serveripoolsest rakendusest andmeid pärida.
Teenuskihti kasutatakse komponentides, kus on vaja liidestuskihist päritud andmeid töödelda
või äriloogikat komponentide vahel jagada.
Tihtipeale teeb server ära andmete töötluse või komponentide vahel jagatavat osa on vähe, siis see kiht on puudu.
Lisades olevatel joonistel~\ref{fig:frontend-code-html}
ja~\ref{fig:frontend-code-ts} on näha koodinäiteid kasutajaliidesest.

Kasutajaliides taaskasutab suures osas töötukassa enda rakenduste jaoks mõeldud kuvaelemente.
Sellised kuvaelemendid ja stiil on viidud ühisesse teeki, mida kasutavad mitu töötukassa rakendust.
Autor otsustas ühist teeki taaskasutada, et rakendus näeks teistele töötukassa rakendustega võimalikult sarnane välja.
Sellise lähenemise eeliseks on, et kasutajad on juba tuttavad, kuidas kuvad ja elemendid toimivad.
Tulevased arendajad on sarnase koodiga tuttavad ja oskavad vajadusel täiendusi teha.
Kuvaelementide taaskasutamine oluliselt kiirendas ning lihtsustas kuvade valmimist,
kuid samal ajal kitsendas valikuid, millised need kuvad saaksid välja näha.
Eelnevalt väljatoodud joonistel~\ref{fig:frontend-teenused}
ja~\ref{fig:frontend-request-response} on näited kasutajaliidese kuvadest.

\subsubsection{Server veebiraamistikuga Quarkus} \label{subsubsec:architecture-backend}

Veebirakenduse serveripoolne osa on ehitatud raamistikule Quarkus~\cite{quarkus}.
See on vabavaraline Red Hati sponsoreeritud raamistik veebirakenduse ehitamiseks Java keeles.
Autor kasutas Quarkuse 1.13.3.~versiooni.
Quarkus võimaldab serverit üles ehitada eraldiseisvate komponentide ja kihtidena,
kus teenuse-, äri- ja andmebaasikiht on omavahel selgelt eraldatud.
Autor valis serveripoolseks raamistikuks Quarkuse, sest see on üles ehitatud olemasolevatele standarditele,
seda on kerge üles seada ning võrreldes teiste raamistikega on see märgatavalt kiirem~\cite{techempower-benchmarks}.
EMPIS-e projekti seisukohalt on oluline, et loodav rakendus oleks võimalikult sarnane projektis olevate rakendustega,
et seda oleks tulevikus võimalik ka teistel arendajatel edasi arendada.
Kuigi projektis kasutatavad rakendused on valdavalt ehitatud Spring raamistikule,
siis Quarkus on sellele väga sarnane ehk kasutab suures osas samu tehnoloogiaid ja ühildub Springi koodiga.
Joonisel~\ref{fig:backend-tech} on välja toodud serveri poolel kasutatavad tehnoloogiad.

\begin{figure}[H]
    \centering
    \includesvg[inkscapelatex=false, width=\textwidth]{assets/image/backend-tech}
    \caption{Serveris kasutatavad tehnoloogiad}
    \label{fig:backend-tech}
\end{figure}

\newpage

Serveripoolne kood on jaotatud teenuse-, äri- ja andmebaasikihtideks ning igas kihis võib olla mitu komponenti.
Teenusekiht kirjeldab ära, milliseid \gls{REST}-päringuid rakendus peab teenindama, tegeleb sisendi valideerimisega
ning seejärel annab sissetuleva päringu edasi ärikihile.
Teenusekiht on üles ehitatud \gls{JAX-RS} standardile.
Ärikihis on kirjeldatud rakenduse loogika, mis võib näiteks sisendi põhjal andmebaasikihist andmeid pärida,
neid töödelda ning teenusekihti tagastada.
Kuna serveri pool toimib suures osas \gls{CRUD}-rakendusena ehk pärib andmeid andmebaasist
või salvestab sinna vastavalt sisendile, siis on ärikihi loogikat vähe.
Andmebaasikiht on ehitatud Hibernate'i standardile ning võimaldab ärikihil andmebaasist pärida
ja sinna salvestada andmeid.
Lisades olevatel joonistel~\ref{fig:backend-resource-code},~\ref{fig:backend-service-code}
ja~\ref{fig:backend-repository-code} on näha koodinäiteid teenuse-, äri- ja andmebaasikihtidest.

Lisaks serveripoolsele rakendusele on üles seatud PostgreSQL 12 andmebaas~\cite{postgresql}.
Autor valis andmebaasiks PostgreSQL-i, sest see on vabavaraline, põhjalikult dokumenteeritud ning
EMPIS-e projektis kasutusel.
Lisades oleval joonisel~\ref{fig:database-schema} on näha rakenduse kasutatav andmebaasi struktuur.
Andmebaasi loomiseks on kasutatud Liquibase'i teeki,
millega serveri käivitamisel jooksutatakse kaasa pandud \gls{SQL}-skripte ning luuakse vajalikud andmebaasitabelid.

\subsection{Veebirakenduse majutus} \label{subsec:architecture-routing}

Veebirakenduse puhul on kasutajaliidese ja serveri kõrval oluline ka nende majutus keskkonnas.
EMPIS-e projektis on rakendused üles seatud \glslink{Docker}{Dockeri} konteineritena
majasiseses \glslink{Kubernetes}{Kubernetese} klastris, kus majutatakse nelja testkeskkonda.
\gls{Docker} võimaldab luua rakenduse käitamiseks isoleeritud põhja,
kus on vajalikud teegid juba paigaldatud ning seadistused tehtud.
Projektisiseselt on nii kasutajaliidese kui ka serveri jaoks \glslink{Docker}{Dockeri} põhjad varasemalt olemas.
\gls{Kubernetes} võimaldab konteinerite käitamist, skaleerimist, võrgusuhtlust
ja muid parameetreid seadistada konfiguratsioonifailidega.
Sarnaselt projektisisestele rakendustele on töös valminud veebirakenduse kasutajaliides
ja server pakendatud \glslink{Docker}{Dockeri} konteineritena
ning need on paigaldatud igasse testkeskkonda \glslink{Kubernetes}{Kubernetese} klastris.
Nelja rakenduse peale on üks ühine andmebaas, mida hoitakse eraldi masinas.
Kuigi \glslink{Kubernetes}{Kuberneteses} on võimalik andmebaasi jaoks kettaruumi tekitada,
on selle eraldi masinas hoidmine töökindlam olnud.
Rakendused on privaatvõrgus kasutajatele kättesaadavaks tehtud, kasutades \glslink{Traefik}{Traefiku} marsruutimist.
