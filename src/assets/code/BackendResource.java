package ee.tk.mock.route.service;

//import ...

@Authenticated
@Tag(name = "route")
@Path("be/route")
public class RouteResource {
  @Inject
  RouteValidator routeValidator;
  @Inject
  RouteService routeService;

  @GET
  @Path("{id}")
  @Produces(APPLICATION_JSON)
  public Optional<Route> getRoute(@PathParam("id") long id) {
    return routeService.get(id);
  }

  @POST
  @Consumes(APPLICATION_JSON)
  @Produces(TEXT_PLAIN)
  @Transactional
  public long createRoute(@NotNull @Valid Route route) {
    routeValidator.validateRouteUniqueByPathAndIdCode(route);
    return routeService.create(route);
  }

  @PUT
  @Consumes(APPLICATION_JSON)
  @Produces(TEXT_PLAIN)
  @Transactional
  public long updateRoute(@NotNull @Valid Route route) {
    return routeService.update(route);
  }
}
