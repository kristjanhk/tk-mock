\section{Teenuste testimine Eesti Töötukassa infosüsteemis EMPIS} \label{sec:problem}

Järgnevas peatükis kirjeldatakse Eesti Töötukassa infosüsteemi,
Nortali osa selle arendamises ja tuuakse välja, kuidas süsteemi arhitektuur on aja jooksul muutnud.
Seejärel kirjeldatakse teenuste testimise metoodikat ning tuuakse välja,
kuidas süsteemi kasvamine on testimist keerulisemaks muutnud.

\subsection{Eesti Töötukassa infosüsteem EMPIS} \label{subsec:problem-overview}

Eesti Töötukassa (edaspidi töötukassa) infosüsteem EMPIS (ingl Estonian Employment Information System)
ehk Eesti Tööhõive Infosüsteem võeti kasutusele 2009.~aastal.
2019.~aasta riigihanke dokumentide~\cite{empis-2019} järgi
on selle eesmärgiks hõlbustada rakendust kasutavate konsultantide
ja menetlejate tööd, süstematiseerides nende tööprotsesse.
Algselt loodi infosüsteem, et toime tulla märkimisväärselt suurenenud töötute arvuga, kui 2008.
aasta algusest 2009.~aasta lõpuks kasvas see 20~000-lt ligi 100~000-ni.
Aastal 2009 avaldatud uudises~\cite{empis-uudis-2019} on kirjas,
et esialgu võimaldas infosüsteem peamiselt hallata töötuna arvelevõtmist, töötutoetuse arvestamist,
töötuskindlustushüvitise vastuvõtmist ja töötukassasse pöördumisi.
EMPIS võimaldas avaldusi ja otsuseid digitaalselt menetleda ning allkirjastada.
Uudisest selgub veel, et infosüsteem oli selleks ajaks liidestatud nelja erineva registriga,
kust oli võimalik pärida töötukassasse pöördunud isikute kohta andmeid.
Riigihanke dokumentides~\cite{empis-2019} tuuakse välja,
et aja jooksul on toetuste ja teenuste arv, mida töötukassa osutab, vähemalt kahekordistunud.
Lisaks töötutele suunatud teenustele aidatakse tööd säilitada ja pakutakse karjäärinõustamist nii vähenenud töövõimega,
töötavatele kui ka õpingutel olevatele inimestele.
Infosüsteem pakub samuti palju asutusesiseseid tugifunktsioone teenuste ja toetuste osutamiseks ning haldamiseks.

Nortal on EUIF (ingl Estonian Unemployment Insurance Fund)
projekti raames arendanud töötukassa infosüsteemi EMPIS aastast 2008~\cite{continuous-delivery-empis-2012}.
Riigihanke dokumentides~\cite{empis-2019} on kirjas,
et EMPIS on ehitatud \glslink{monolith}{monoliitse rakendusena} Spring ning Aranea Web Framework raamistikega.
Rakenduse \glslink{esitluskiht}{esitlus-}, teenus-, äri-
ja \glslink{andmebaasikiht}{andmebaasikihid} on omavahel tihedalt seotud.
Aja jooksul on rakendust järjest täiendatud ja praeguseks on see kasvanud üle miljoni koodirea
\footnote{Repositooriumis väljavõtte alusel, mis on välja toodud lisades oleval joonisel~\ref{fig:empis-statistic}}.
Alates aastast 2018 on viidud funktsionaalsust üle väiksemateks rakendusteks ehk mooduliteks,
et lihtsustada süsteemi arhitektuuri ning selle arendamist.
Infosüsteemi majutuse hanke dokumentidest~\cite{majutus-2019} tuleb välja,
et tihedalt seotud kihtide asemel liigutakse \glslink{mikro-arch}{mikroteenuste arhitektuuri} poole,
kus äriloogilised osad on omavahel selgelt eristatud ning nendevaheline suhtlus toimib üle teenuste.
Näiteks aastal 2020 loodi Isikuandmete moodul (edaspidi ISIK),
mille rakendusse ja andmebaasi viidi isikutega seotud äriloogika ja
andmed, et isikute andmed ja nendega seotud tegevused oleksid loogiliselt eraldatud ülejäänud süsteemist.

Algselt koosnes töötukassa infosüsteem ühest rakendusest,
mis suhtles vaid nelja registriga üle andmevahetuskihi \glslink{xroad}{X-tee}~\cite{empis-uudis-2019}.
2019.~aasta infosüsteemi majutuse hanke dokumentidest~\cite{majutus-2019} selgub,
et aastal 2020 koosneb kogu töötukassa infosüsteem 20 moodulist,
millest kümme on Nortali arenduses ja ülejäänud koostööpartnerite halduses.
Nortali arendatavad moodulid ja nendevaheline suhtlus on välja toodud joonisel~\ref{fig:nortal-modules}.

\begin{figure}[!htb]
    \centering
    \includesvg[inkscapelatex=false, width=\textwidth]{assets/image/empis-moodulid}
    \caption{Nortali hallatavad moodulid}
    \label{fig:nortal-modules}
\end{figure}

Aastal 2020 suhtleb infosüsteem kokku 21 erineva \glslink{xroad}{X-tee} andmekoguga~\cite{xtee-andmed}.
Iga moodul ja register võib koosneda kümnetest teenustest, mida võib kasutada mitu rakendust.
See tähendab, et kuigi äriloogika on paremini üksteisest eraldatud,
siis erinevate komponentide integratsioon on muutunud märkimisväärselt keerulisemaks~\cite{majutus-2019}.
\glslink{xroad}{X-tee} avalike andmete~\cite{xtee-andmed, xtee-andmed-scraper} põhjal kasutas töötukassa infosüsteem
2020.~aasta novembris 53 erinevat \glslink{xroad}{X-tee} teenust, tehes nendesse keskmiselt 63~000 päringut päevas.
Rakenduste logide põhjal tehti moodulite vahel samal perioodil keskmiselt 1~878 000 päringut päevas
\footnote{Rakenduste logide väljavõte, mis on toodud välja lisades oleval joonisel~\ref{fig:empis-request-statistic}}.

Äriloogika eraldamine mooduliteks lihtsustab nende testimist, sest ühte osa saab teistest sõltumatult testida.
Aastal 2011 on Polina Morozova~\cite{testimine-empis-2011}
oma magistritöös kirjeldanud testimist EMPIS-e projekti näitel.
Kui äriloogika eraldatus on lihtsustanud äriloogika enda testimist,
siis rakenduse tükeldamine suurendab teenuste arvu
ja seega ka erinevate komponentide vahelise suhtluse ehk integratsiooni testimist.
Eriti keerukaks on muutnud mitut moodulit hõlmavate protsesside testimine.
Dmitrii Savchenko~\cite{savchenko-2019} on välja toonud, et integratsiooni
ja tööprotsesside testimise lihtsustamiseks kasutatakse tööriistu,
millega on võimalik teenuste päringute vastused asendada näidisvastustega.
Sellist testimismetoodikat nimetatakse ka \glslink{conformance-testing}{vastavustestimiseks}.

\newpage
\subsection{Testimismetoodika ja probleemid teenuste testimisel} \label{subsec:problem-testing}

Jyri Lehvä jt~\cite{contract-testing-case-study} on kirjutanud,
et vastavus- ehk integratsioonitestimine (ingl \emph{conformance testing}) on testimismetoodika,
mida kasutatakse rakendustevahelise suhtlemise kontrollimiseks.
Selle metoodika põhieesmärk on teenust pakkuva rakenduse (edaspidi teenusepakkuja)
ja teenust tarbiva rakenduse (edaspidi teenusetarbija) vahele luua leping ehk spetsifikatsioon,
mis kirjeldab ära teenusetarbija tehtavad päringud ning pakkuja vastused.
Kui osapoolte vahel on leping loodud,
siis on võimalik nii teenusepakkujat kui ka -tarbijat teineteisest sõltumatult testida.
Spetsifikatsioon toimib teenusekihtide eraldajana, mistõttu pole integratsiooni
kontrollimiseks vaja kõiki rakendusi ja nende sõltuvusi üles seada.
See tähendab ka, et teenusepakkuja ning -tarbija peavad loodud lepingust kinni pidama.
Autorid rõhutavad, et kui ühte poolt täiendatakse,
siis peavad need muudatused kajastuma ka spetsifikatsioonis ning omakorda teises osapooles.
Dmitrii Savchenko~\cite{savchenko-2019} on välja toonud,
et lepingupõhist testimismetoodikat kasutatakse eelkõige rakendustega,
mis on üles ehitatud mikroteenuste arhitektuurile.
Autor toob samuti välja, et see võimaldab rakenduste äriloogilisi osi täiendada eraldiseisvalt, ilma
et ülejäänud rakendus sellest sõltuks.
Kui lisaks äriloogikale täiendatakse pakutavat teenust,
siis lepingupõhise testimise kohaselt peab täiendama ka spetsifikatsiooni
ja vajadusel teenuse tarbijat~\cite{contract-testing-case-study}.

Töötukassa infosüsteemis suhtlevad rakendused üksteisega üle teenuste.
\glslink{xroad}{X-tee} alamsüsteemide lehelt~\cite{xtee-alamsysteemid}
ja 2019.~aasta majutuse hanke dokumentidest~\cite{majutus-2019} on näha,
et süsteemi moodulid liidestuvad üksteisega peamiselt \gls{REST}-teenustega,
mis koosnevad sünkroonsetest \gls{HTTP}-päringutest ja vastustest, mille andmete kuju vastab \gls{JSON} standardile.
Joonisel~\ref{fig:http-example} on näha näidet ISIK mooduli isikuandmete päringust ja vastusest.
Alternatiivina kasutatakse ka \gls{AMQP}-liidestust,
mis võimaldab rakendustevahelist asünkroonset sõnumitevahetust ja nende sõnumite kuju on samuti \gls{JSON}-tüüpi.
\glslink{xroad}{X-tee} registritega suhtlus toimub üle \gls{SOAP}-teenuste,
mis sarnaselt \gls{REST}-teenustele toimub samuti üle HTTP-päringute,
kuid andmete kuju vastab \gls{XML} standardile.

\begin{figure}[!htb]
    \centering
    \includesvg[inkscapelatex=false, width=\textwidth]{assets/image/http-example}
    \caption{ISIK mooduli isikuandmete REST päring ja selle vastus}
    \label{fig:http-example}
\end{figure}

\newpage

Integratsioonitestimise teeb EMPIS-e projektis keerukaks asjaolu, et alati ei ole olemas täpset spetsifikatsiooni,
kuidas teenusele saadetav päring ja tagastatav vastus peaksid välja nägema.
\gls{REST}- ja \gls{AMQP}-teenuste puhul on olemas analüütiku loodud dokument, mis sisaldab teenuse kirjeldust ja
mille järgi peab testija olemasolevate teadmiste põhjal päringu või vastuse looma.
\gls{SOAP}-teenuste puhul on olemas \gls{WSDL}-spetsifikatsioonid,
mille järgi on võimalik genereerida vastuse struktuur, kuid mitte andmeid.
Seega alustatakse integratsioonitestimist sellest, et otsitakse rakenduse logist või arendaja käest näidisvastus,
mida saab muuta ja seejärel testimisel kasutada.
Olemasolevaid lahendusi, millega on võimalik rakendustevahelist suhtlust jälgida, muuta
ning näidisvastused välja võtta, kirjeldatakse 2.~peatükis.

Integratsioonitestimise efektiivseks ja põhjalikuks läbiviimiseks on vajalik kahe rakenduse, nimetagem neid A ja B,
vahel olev liidestus ümber seadistada nii, et tehtav päring liiguks testija ülesseatud testimisrakendusse C,
mis vastaks päringule eelseadistatud vastusega, mis oleks sama struktuuriga nagu rakenduse B vastus.
Nii on võimalik läbi proovida, kas moodul A sooritab ootuspärase päringu ja suudab eelseadistatud vastust sisse lugeda.
Kirjeldatud olukord on näha joonisel~\ref{fig:testing-problem}.
Praegu nõuab ümberseadistus konfiguratsioonifaili muudatust moodulis A ja rakenduse taaskäivitamist.
Seejärel suunatakse kõik moodulisse B tehtavad päringud ümber testimisrakendusse C\@.
Keeruliseks teeb olukorra see, et protseduuri peab tegema iga rakenduse kohta eraldi ja
testija peab oskama konfiguratsioonifaili õigesti muuta.
Samuti on rakendus taaskäivitamise ajal kättesaamatu.
Kuna testimise ajal suunatakse kõik päringud testimisrakendusse C,
siis häirib see ka teiste arendajate ja testijate tööd.
Peatükis~3 kirjeldatakse olukorrale lahendust.

\begin{figure}[!htb]
    \centering
    \includesvg[inkscapelatex=false, width=0.35\textwidth]{assets/image/reroute-config}
    \caption{Päringute liikumine moodulite A ja B ning testimisrakenduse C vahel}
    \label{fig:testing-problem}
\end{figure}

\newpage

Töötukassa infosüsteemi testimiseks on projektis üles seatud neli erinevat testkeskkonda,
mis on liidestatud vastu \glslink{xroad}{X-tee} testregistreid ning koostööpartnerite testkeskkondade rakendusi.
Mirjam Rauba~\cite{pidev-tarnimine-empis-2012} on välja toonud, et juba aastal 2012 oli kokku kolm testkeskkonda, kus
igas testkeskkonnas hoiti arendustsükli seisu või kindlat funktsionaalsust, mida plaaniti kliendile tarnida.
Testijad kasutavad ühiseid testkeskkondi,
sest kõigi moodulite ülesseadmine, seadistamine ja uuendamine on küllaltki ajakulukas ning ressursimahukas.
2019.~aasta majutuse hanke dokumentide~\cite{majutus-2019} järgi on koolituskeskkonna,
mis on kõige sarnasem teistele testkeskkondadele, mälunõudlus kuni 64~GB\@.
Testkeskkondade uuendamisega tegeleb paigalduskonveier ning selle haldusega arendajad.
