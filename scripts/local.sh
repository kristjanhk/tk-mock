#!/usr/bin/env bash
# Run in project root directory

mkdir out
inotifywait -m --event modify --format %w out/main.pdf | evince out/main.pdf &

docker build -t latex .

# todo run as host user
docker run -it --rm \
           -v "$(pwd)/out":/home/out \
           -v "$(pwd)/src":/home/src \
           latex
