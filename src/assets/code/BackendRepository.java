package ee.tk.mock.route.service;

//import ...

@ApplicationScoped
public class RouteRepository implements PanacheRepository<RouteEntity> {

  public List<RouteEntity> findByIdCodeAndSystem(String idCode, String system) {
    return list("idCode = ?1 AND system = ?2 ORDER BY time DESC", idCode, system);
  }

  public List<RouteEntity> findByIds(List<Long> ids) {
    return list("id IN ?1", ids);
  }

  public Optional<RouteEntity> getByMethodAndPathAndIdCode(RouteMethod routeMethod,
                                                           String path,
                                                           String idCode) {
    return find("method = ?1 AND path = ?2 AND idCode = ?3", routeMethod, path, idCode)
        .singleResultOptional();
  }
}
