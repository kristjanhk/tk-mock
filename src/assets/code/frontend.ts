import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {Icons, ModalFactory, Util} from "@tk/tk-angular-common";
import {Route} from "../../generated/models/route";
import {MockListComponent} from "../../mock/list/mock-list.component";
import {Mock} from "../../generated/models/mock";

@Component({selector: 'm-route-panel', templateUrl: './route-panel.component.html'})
export class RoutePanelComponent {

    @ViewChild(MockListComponent, {static: true})
    private mockList: MockListComponent;

    @Input() route: Route;
    @Input() mocks: Mock[];
    @Input() collapsed: boolean;
    @Output() onRouteChanged: EventEmitter<void> = new EventEmitter<void>();

    icons: typeof Icons = Icons;
    showFilter: boolean = false;

    constructor(private modalFactory: ModalFactory) {
    }

    getTitle(): string {
        const notes = Util.isPresent(this.route.name) ? ` (${this.route.name})` : '';
        return `${this.route.type} ${this.route.method} ${this.route.path}${notes}`;
    }

    toggleFilter(): void {
        this.showFilter = !this.showFilter;
    }

    createMock(): void {
        this.mockList.createMock();
    }

    openRouteModal(): void {
        //...
    }
}




































