FROM registry.gitlab.com/kristjanhk/tk-mock:base

COPY scripts /home/scripts
CMD ["/bin/bash", "scripts/watch.sh"]
