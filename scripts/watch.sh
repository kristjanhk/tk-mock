#!/usr/bin/env bash

# Mirror src file structure to out
for OUTPUT in $(find src -type d | sed -e 's/src\///g')
do
  mkdir -p out/${OUTPUT}
done

cd src
latexmk -pdf -file-line-error -interaction=nonstopmode \
-synctex=1 -output-format=pdf -output-directory=/home/out -shell-escape -pvc \
-r "/home/config.latexmk" \
-auxdir=/home/src -outdir=/home/out \
main.tex
