#!/usr/bin/env bash
mkdir out
cd src

# todo remove force compilation (-f argument)
# /usr/local/texlive/texmf-dist/tex/generic/oberdiek/hobsub-generic.sty:1395: Package ifluatex Error: Name clash, \ifluatex is already defined.
latexmk -f -pdf -file-line-error -interaction=nonstopmode -synctex=1 -output-format=pdf -shell-escape \
-r "/home/config.latexmk" \
main.tex

cp main.pdf ../out/main.pdf
