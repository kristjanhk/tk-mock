package ee.tk.mock.route.service;

//import ...

@ApplicationScoped
public class RouteService {
  @Inject
  RouteRepository routeRepository;
  @Inject
  RouteMapper routeMapper;

  public Optional<Route> get(long id) {
    return routeRepository.findByIdOptional(id)
        .map(entity -> routeMapper.convert(entity));
  }

  public long create(Route route) {
    route.setTime(LocalDateTime.now());
    RouteEntity entity = routeMapper.inverse(route);
    routeRepository.persist(entity);
    route.setId(entity.getId());
    return entity.getId();
  }

  public long update(Route route) {
    route.setTime(LocalDateTime.now());
    RouteEntity entity = routeMapper.inverse(route);
    return routeRepository.getEntityManager().merge(entity).getId();
  }
}
